FROM centos:8

# Create app directory
WORKDIR /usr/src/qldapm-api

ADD . ./
COPY package.json /usr/src/qldapm-api/

RUN curl -sL https://rpm.nodesource.com/setup_14.x | bash -
RUN dnf install -y nodejs

RUN npm install
COPY . /usr/src/qldapm-api
EXPOSE 4000
CMD [ "npm","run", "dev"]
